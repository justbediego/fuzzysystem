﻿using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;
using WIT2FESS.Core.Business;
using WIT2FESS.Core.DataAccess;
using WIT2FESS.UI.Models;

namespace WIT2FESS.UI.Api
{
    public class AuthenticationController : ApiController
    {
        private GuestBusiness guestBusiness;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
        public AuthenticationController()
        {
            WIT2FESSContext dbContext = new WIT2FESSContext();
            guestBusiness = new GuestBusiness(dbContext);
        }
        public AuthenticationController(WIT2FESSContext dbContext)
        {
            guestBusiness = new GuestBusiness(dbContext);
        }
        [HttpGet]
        public bool IsLoggedIn()
        {
            return !string.IsNullOrEmpty(User.Identity.Name);
        }
        [HttpGet]
        public UserDataDTO GetUserData()
        {
            return new UserDataDTO()
            {
                Username = User.Identity.Name,
                FirstName = ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == "FirstName").Select(c => c.Value).FirstOrDefault(),
                LastName = ((ClaimsIdentity)User.Identity).Claims.Where(c => c.Type == "LastName").Select(c => c.Value).FirstOrDefault()
            };
        }
        [HttpPost]
        public void Login(LoginDTO model)
        {
            var user = guestBusiness.GetUser(model.Username, model.Password);
            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.Username));
            identity.AddClaim(new Claim("FirstName", user.FirstName));
            identity.AddClaim(new Claim("LastName", user.LastName));
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = model.IsPersistent }, identity);
        }
        [HttpGet]
        public void Logout()
        {
            AuthenticationManager.SignOut();
        }
    }
}