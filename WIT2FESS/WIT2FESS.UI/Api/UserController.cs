﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WIT2FESS.Core.Business;
using WIT2FESS.Core.DataAccess;
using WIT2FESS.UI.Models;
using WIT2FESS.Core.Model;
using System.Threading.Tasks;
using System.Web;
using System.IO;

namespace WIT2FESS.UI.Api
{
    public class UserController : ApiController
    {
        private WIT2FESSContext dbContext;
        private UserBusiness userBusiness;
        private AuthenticationController authenticationController;
        public UserController()
        {
            dbContext = new WIT2FESSContext();
            userBusiness = new UserBusiness(dbContext);
            authenticationController = new AuthenticationController(dbContext);
        }
        private string username
        {
            get
            {
                return authenticationController.GetUserData().Username;
            }
        }
        private string matFileFolder
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/MatFiles/");
            }
        }
        private string evaluationFile
        {
            get
            {
                return Path.Combine(matFileFolder, "Evaluation_" + username + ".csv");
            }
        }
        [HttpPost]
        public void EditOrAddSystem(EditOrAddSystemDTO model)
        {
            userBusiness.EditOrAddSystem(username,
                model.SystemId,
                model.SystemName,
                (FIS.AndMethods)model.AndMethod,
                (FIS.OrMethods)model.OrMethod,
                (FIS.ImplicationMethods)model.ImplicationMethod,
                (FIS.AggregationMethods)model.AggregationMethod,
                (FIS.DefuzzificationMethods)model.DefuzzificationMethod,
                (FIS.TypeReductionMethods)model.TypeReductionMethod);
        }
        [HttpPost]
        public void RemoveSystem(DeleteObjDTO model)
        {
            userBusiness.RemoveSystem(username, model.ObjectID);
        }
        [HttpPost]
        public void EditOrAddVariable(EditOrAddVariableDTO model)
        {
            userBusiness.EditOrAddVariable(username,
                model.VariableId,
                model.SystemId,
                model.VariableName,
                //model.VariableIndex,
                (Variable.VariableTypes)model.VariableType,
                model.RangeMax,
                model.RangeMin);
        }
        [HttpPost]
        public void RemoveVariable(DeleteObjDTO model)
        {
            userBusiness.RemoveVariable(username, model.ObjectID);
        }
        [HttpPost]
        public void EditOrAddMF(EditOrAddMFDTO model)
        {
            userBusiness.EditOrAddMF(username,
                model.MFId,
                model.VariableId,
                model.MFName,
                (MF.MFTypes)model.MFType,
                model.Intervals,
                model.FOU);
        }
        [HttpPost]
        public void RemoveMF(DeleteObjDTO model)
        {
            userBusiness.RemoveMF(username, model.ObjectID);
        }
        [HttpPost]
        public int AddARule(AddRuleDTO model)
        {
            return userBusiness.AddARule(username, model.SystemId);
        }
        [HttpPost]
        public void RemoveRule(DeleteObjDTO model)
        {
            userBusiness.RemoveRule(username, model.ObjectID);
        }
        [HttpPost]
        public void AddMFToRule(AddOrRemoveRMFDTO model)
        {
            userBusiness.AddMFToRule(username, model.RuleId, model.MFId);
        }
        [HttpPost]
        public void RemoveMFFromRule(AddOrRemoveRMFDTO model)
        {
            userBusiness.RemoveMFFromRule(username, model.RuleId, model.MFId);
        }
        [HttpGet]
        public object GetAllSystems()
        {
            return userBusiness.GetAllSystems(username);
        }
        [HttpGet]
        public object GetSystemByID(int systemId)
        {
            return userBusiness.GetSystemByID(username, systemId);
        }
        [HttpGet]
        public object GetAllVariables(int systemId)
        {
            return userBusiness.GetAllVariables(username, systemId);
        }
        [HttpPost]
        public async Task UploadEvaluationData()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            if (File.Exists(evaluationFile))
                File.Delete(evaluationFile);
            File.WriteAllBytes(evaluationFile, file.ReadAsByteArrayAsync().Result);
        }
        [HttpGet]
        public object GetVariableByID(int variableId)
        {
            return userBusiness.GetVariableByID(username, variableId);
        }
        [HttpGet]
        public object GetMFByID(int mfId)
        {
            return userBusiness.GetMFByID(username, mfId);
        }
        [HttpGet]
        public object GetAllRules(int systemId)
        {
            return userBusiness.GetAllRules(username, systemId);
        }
        [HttpPost]
        public void CreateSystem(SystemCreationDTO model)
        {
            userBusiness.CreateSystemInMatlab(username, matFileFolder, model.SystemId);
        }
        [HttpPost]
        public object EvaluateDataInMatlab(SystemCreationDTO model)
        {
            return userBusiness.EvaluateDataInMatlab(username, matFileFolder, model.SystemId);
        }
    }
}
