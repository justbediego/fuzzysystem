﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WIT2FESS.Core.Business;
using WIT2FESS.Core.DataAccess;
using WIT2FESS.UI.Models;

namespace WIT2FESS.UI.Api
{
    public class GuestController : ApiController
    {
        private WIT2FESSContext dbContext;
        private GuestBusiness guestBusiness;
        private AuthenticationController authenticationController;
        public GuestController()
        {
            dbContext = new WIT2FESSContext();
            guestBusiness = new GuestBusiness(dbContext);
            authenticationController = new AuthenticationController(dbContext);
        }
        [HttpPost]
        public void CreateUser(UserCreationDTO model)
        {
            guestBusiness.CreateUser(model.Username, model.Password, model.FirstName, model.LastName);
            authenticationController.Login(new LoginDTO()
            {
                Username = model.Username,
                Password = model.Password,
                IsPersistent = false,
            });
        }
    }
}
