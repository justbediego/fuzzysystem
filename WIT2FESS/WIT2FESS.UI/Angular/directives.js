﻿angular.module('wit2fess')
    .directive('fisChartjs', function () {
        return {
            scope: {
                chartdata: '=chartdata',
                options: '=options',
                type: '@',
            },
            restrict: 'A',
            link: function (scope, el, attrs) {
                scope.Render = function () {
                    var ctx = el[0].getContext("2d");
                    new Chart(ctx, { type: scope.type, data: scope.chartdata, options: scope.options });
                };
                scope.Render();
            }
        };
    })
    .directive('activeFileUpload', function () {
        return {
            scope: {
                actionUrl: '@',
                title: '@',
                success: "&",
                start:"&",
            },
            restrict: 'E',
            template: '<div class="progress"><div class="progress-bar progress-bar-primary"></div></div><span class="badge badge-primary fileinput-button"><span>{{title}}</span><input class="fileupload" type="file" name="files"></span>',
            link: function (scope, el, attrs) {
                scope.isStarted = false;
                scope.title = attrs.title;
                el.find(".progress").hide();
                el.find(".progress .progress-bar").css('width', 0);
                el.find(".fileupload").fileupload({
                    url: attrs.actionUrl,
                    formData: {
                        Id: attrs.data,
                    },
                    done: function () {
                        scope.isStarted = false;
                        el.find(".fileinput-button").show();
                        el.find(".progress").hide();
                        el.find(".progress .progress-bar").css('width', 0);
                        scope.success();
                    },
                    error: function (response) {
                        scope.isStarted = false;
                        el.find(".fileinput-button").show();
                        el.find(".progress").hide();
                        el.find(".progress .progress-bar").css('width', 0);
                        alert(response.responseJSON.ExceptionMessage);
                    },
                    progressall: function (e, data) {
                        if (!scope.isStarted) {
                            scope.start();
                            scope.isStarted = true;
                        }
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        el.find(".fileinput-button").hide();
                        el.find(".progress").show();
                        el.find(".progress .progress-bar").css('width', progress + '%');
                    }
                });
            }
        }
    })
    .directive('ngReturn', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngReturn);
                    });
                    event.preventDefault();
                }
            });
        };
    })
    .directive('includeReplace', function () {
        return {
            require: 'ngInclude',
            restrict: 'A', /* optional */
            link: function (scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    })
    .directive('viewerjs', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                $(el).viewer({ navbar: false });
            }
        }
    });