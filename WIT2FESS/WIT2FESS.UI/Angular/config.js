﻿angular.module('wit2fess').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$ocLazyLoadProvider','IdleProvider',function ($stateProvider, $urlRouterProvider, $locationProvider, $ocLazyLoadProvider, IdleProvider) {
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds
    $locationProvider.hashPrefix('');
    $ocLazyLoadProvider.config({
        debug: false,
    });
    $urlRouterProvider.otherwise('/');
    //states
    $stateProvider
        .state('home', {
            url: '/',
            controller: 'homeCtrl',
            templateUrl: '/Statics/home.html'
        })
        .state('register', {
            url: '/Register',
            controller: 'registerCtrl',
            templateUrl: '/Statics/register.html'
        })
        //User
        .state('user', {
            url: '/User',
            abstract: true,
            controller: 'profileCtrl',
            templateUrl: '/Statics/profile.html'
        })
        .state('user.home', {
            url: '/Home',
            //controller: 'profileCtrl',
            templateUrl: '/Statics/User/home.html'
        })
        .state('user.fuzzy1', {
            url: '/Fuzzy1',
            //controller: 'profileCtrl',
            templateUrl: '/Statics/User/fuzzy1.html'
        })
        .state('user.fuzzy2', {
            url: '/Fuzzy2',
            //controller: 'profileCtrl',
            templateUrl: '/Statics/User/fuzzy2.html'
        })
        .state('user.fuzzy3', {
            url: '/Fuzzy3',
            //controller: 'profileCtrl',
            templateUrl: '/Statics/User/fuzzy3.html'
        })
        .state('user.systems', {
            url: '/Systems',
            controller: 'systemsCtrl',
            templateUrl: '/Statics/User/systems.html',            
        })
        .state('user.evaluation', {
            url: '/Evaluation',
            controller: 'evaluationCtrl',
            templateUrl: '/Statics/User/evaluation.html',
        })
        ;
}]).run(['$rootScope','$state',function ($rootScope, $state) {
    $rootScope.$state = $state;
}]);