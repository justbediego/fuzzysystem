﻿var app = angular.module('wit2fess', [
         'ui.router',                    // Routing
         'oc.lazyLoad',                  // ocLazyLoad
         'ui.bootstrap',                 // Ui Bootstrap
         'ngSanitize',
         'ngRoute',
         'toaster',
         'ngIdle'
]);