﻿angular.module('wit2fess').service('services', ['$http', '$uibModal', 'toaster', function ($http, $uibModal, toaster) {
    var vm = this;
    //************COMMON*********************
    vm.ShowAlertModal = function (title, text) {
        $uibModal.open(
            {
                templateUrl: '/Statics/Modals/alertModal.html',
                animation: true,
                controller: function ($scope) {
                    $scope.title = title;
                    $scope.text = text;
                    $scope.Close = function () {
                        this.$close();
                    }
                },
            });
    };
    vm.ShowError = function (title, text) {
        toaster.error(title, text);
    };
    vm.ShowSuccess = function (title, text) {
        toaster.success(title, text);
    }

    //************CALLER************
    function CallServer(method, location, data, success, fail) {
        switch (method) {
            case 'post':
                $http.post(location, data).then(function (result) {
                    if (success)
                        success(result);
                }, function (e) {
                    vm.ShowError('Server', e.data.ExceptionMessage);
                    if (fail)
                        fail();
                });
                break;
            case 'get':
                $http.get(location, { params: data }).then(function (result) {
                    if (success)
                        success(result);
                }, function (e) {
                    vm.ShowError('Server', e.data.ExceptionMessage);
                    if (fail)
                        fail();
                });
                break;
        }
    }

    //************AUTHENTICATION************
    vm.IsLoggedIn = function (success, fail) {
        CallServer('get', '/api/Authentication/IsLoggedIn', null, success);
    };
    vm.GetUserData = function (success, fail) {
        CallServer('get', '/api/Authentication/GetUserData', null, success);
    };
    vm.Login = function (data, success, fail) {
        CallServer('post', '/api/Authentication/Login', data, success, fail);
    };
    vm.Logout = function (success, fail) {
        CallServer('get', '/api/Authentication/Logout', null, success);
    };
    vm.CreateUser = function (data, success, fail) {
        CallServer('post', '/api/Guest/CreateUser', data, success, fail);
    };

    //************USER************
    vm.EditOrAddSystem = function (data, success, fail) {
        CallServer('post', '/api/User/EditOrAddSystem', data, success, fail);
    }
    vm.GetAllSystems = function (success, fail) {
        CallServer('get', '/api/User/GetAllSystems', null, success, fail);
    }
    vm.GetSystemByID = function (systemId, success, fail) {
        CallServer('get', '/api/User/GetSystemByID', { systemId: systemId }, success, fail);
    }
    vm.RemoveSystem = function (systemId, success, fail) {
        CallServer('post', '/api/User/RemoveSystem', { ObjectID: systemId }, success, fail);
    }
    vm.GetAllVariables = function (systemId, success, fail) {
        CallServer('get', '/api/User/GetAllVariables', { systemId: systemId }, success, fail);
    }
    vm.GetVariableByID = function (variableId, success, fail) {
        CallServer('get', '/api/User/GetVariableByID', { variableId: variableId }, success, fail);
    }
    vm.EditOrAddVariable = function (data, success, fail) {
        CallServer('post', '/api/User/EditOrAddVariable', data, success, fail);
    }
    vm.RemoveVariable = function (variableId, success, fail) {
        CallServer('post', '/api/User/RemoveVariable', { ObjectID: variableId }, success, fail);
    }
    vm.EditOrAddMF = function (data, success, fail) {
        CallServer('post', '/api/User/EditOrAddMF', data, success, fail);
    }
    vm.RemoveMF = function (mfId, success, fail) {
        CallServer('post', '/api/User/RemoveMF', { ObjectID: mfId }, success, fail);
    }
    vm.GetMFByID = function (mfId, success, fail) {
        CallServer('get', '/api/User/GetMFByID', { mfId: mfId }, success, fail);
    }
    vm.GetAllRules = function (systemId, success, fail) {
        CallServer('get', '/api/User/GetAllRules', { systemId: systemId }, success, fail);
    }
    vm.AddARule = function (systemId, success, fail) {
        CallServer('post', '/api/User/AddARule', { systemId: systemId }, success, fail);
    }
    vm.RemoveRule = function (ruleId, success, fail) {
        CallServer('post', '/api/User/RemoveRule', { ObjectID: ruleId }, success, fail);
    }
    vm.AddMFToRule = function (data, success, fail) {
        CallServer('post', '/api/User/AddMFToRule', data, success, fail);
    }
    vm.RemoveMFFromRule = function (data, success, fail) {
        CallServer('post', '/api/User/RemoveMFFromRule', data, success, fail);
    }
    vm.CreateSystem = function (data, success, fail) {
        CallServer('post', '/api/User/CreateSystem', data, success, fail);
    }
    vm.EvaluateDataInMatlab = function (data, success, fail) {
        CallServer('post', '/api/User/EvaluateDataInMatlab', data, success, fail);
    }    
}]);