﻿angular.module("wit2fess").controller('systemsCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.systems = [];
    vm.ReloadSystems = function () {
        services.GetAllSystems(function success(result) {
            vm.systems = result.data;
        });
    }
    vm.ReloadSystems();
    vm.RemoveSystem = function (systemId) {
        services.RemoveSystem(systemId, function success() {
            vm.ReloadSystems();
        });
    }
    vm.OpenEditOrAddSystem = function (systemId) {
        $uibModal.open({
            templateUrl: '/Statics/User/Modals/editOrAddSystem.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.system = {
                    AndMethod: "0",
                    OrMethod: "0",
                    ImplicationMethod: "0",
                    AggregationMethod: "0",
                    DefuzzificationMethod: "0",
                    TypeReductionMethod: "0"
                }
                if (systemId) {
                    services.GetSystemByID(systemId, function (result) {
                        vm2.system = result.data;
                    });
                }
                vm2.EditOrAddSystem = function () {
                    services.EditOrAddSystem(vm2.system, function success() {
                        vm2.Close();
                        vm.ReloadSystems();
                    });
                }
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
    vm.variables = {};
    vm.rules = {};
    vm.ShowVariables = function (systemId) {
        services.GetAllVariables(systemId, function success(result) {
            vm.variables[systemId] = result.data ? result.data : [];
        });
        services.GetAllRules(systemId, function success(result) {
            vm.rules[systemId] = result.data ? result.data : [];
        });
    }
    vm.HideVariables = function (systemId) {
        vm.variables[systemId] = null;
    }
    vm.OpenEditOrAddVariable = function (systemId, variableId) {
        $uibModal.open({
            templateUrl: '/Statics/User/Modals/editOrAddVariable.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.variable = {
                    systemId: systemId,
                    VariableType: "0"
                }
                if (variableId) {
                    services.GetVariableByID(variableId, function success(result) {
                        vm2.variable = result.data;
                        vm2.variable.systemId = systemId;
                        vm2.variable.variableId = variableId;
                    });
                }
                vm2.EditOrAddVariable = function () {
                    services.EditOrAddVariable(vm2.variable, function success(result) {
                        vm2.Close();
                        vm.ShowVariables(systemId);
                    });
                }
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
    vm.RemoveVariable = function (systemId, variableId) {
        services.RemoveVariable(variableId, function success() {
            vm.ShowVariables(systemId);
        });
    }
    vm.OpenEditOrAddMF = function (systemId, variableId, mfId) {
        $uibModal.open({
            templateUrl: '/Statics/User/Modals/editOrAddMF.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.MF = {
                    variableId: variableId,
                    MFType: "0"
                }
                if (mfId) {
                    services.GetMFByID(mfId, function success(result) {
                        vm2.MF = result.data;
                        vm2.MF.VariableId = variableId;
                        vm2.MF.MFId = mfId;
                    });
                }
                vm2.EditOrAddMF = function () {
                    services.EditOrAddMF(vm2.MF, function success(result) {
                        vm2.Close();
                        vm.ShowVariables(systemId);
                    });
                }
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
    vm.RemoveMF = function (systemId, mfId) {
        services.RemoveMF(mfId, function success() {
            vm.ShowVariables(systemId);
        });
    }
    vm.AddARule = function (systemId) {
        services.AddARule(systemId, function success() {
            vm.ShowVariables(systemId);
        });
    }
    vm.RemoveRule = function (systemId, ruleId) {
        services.RemoveRule(ruleId, function success() {
            vm.ShowVariables(systemId);
        });
    }
    vm.OpenAddRelation = function (systemId, ruleId) {
        $uibModal.open({
            templateUrl: '/Statics/User/Modals/addRuleMF.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.RuleMF = {
                    RuleId: ruleId,
                    MFId: null,
                    VariableId: undefined,
                };
                vm2.variables = [];
                services.GetAllVariables(systemId, function success(result) {
                    vm2.variables = result.data ? result.data : [];
                });
                vm2.MFs = [];
                vm2.VariableSelected = function () {
                    vm2.RuleMF.MFId = null;
                    for (var i = 0; i < vm2.variables.length; i++)
                        if (vm2.variables[i].VariableID == vm2.RuleMF.VariableId)
                            vm2.MFs = vm2.variables[i].MFList;
                }
                vm2.AddMFToRule = function () {
                    if (vm2.RuleMF.MFId && vm2.RuleMF.RuleId)
                        services.AddMFToRule(vm2.RuleMF, function success(result) {
                            vm2.Close();
                            vm.ShowVariables(systemId);
                        });
                }
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
    vm.RemoveRMF = function (systemId, ruleId, mfId) {
        services.RemoveMFFromRule({ ruleId: ruleId, mfId: mfId }, function success(result) {
            vm.ShowVariables(systemId);
        });
    }
});