﻿angular.module("wit2fess").controller('evaluationCtrl', function ($scope, services, $location) {
    var vm = $scope;
    vm.systems = [];
    vm.evaluation = {};
    vm.Initialize = function () {
        vm.evaluation = {
            isReady: false,
            analyzationState: 'start',
        };
    }
    vm.resultChart = {
        chartdata: {
            labels: [],
            datasets: [
                {
                    label: "True Value",
                    lineTension: 0,
                    backgroundColor: 'rgba(26,179,148,0.5)',
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",
                    data: []
                }, {
                    label: "Estimation",
                    lineTension: 0,
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    pointBorderColor: "#fff",
                    data: []
                }
            ],
        },
        options: {
            responsive: true
        },
    };

    vm.ReloadSystems = function () {
        services.GetAllSystems(function success(result) {
            vm.systems = result.data;
        });
    }
    vm.UploadStarted = function () {
        vm.$apply(function () {
            vm.evaluation.isReady = false;
        });
    }
    vm.UploadSuccessed = function () {
        vm.$apply(function () {
            vm.evaluation.isReady = true;
        });
    }
    vm.ReloadSystems();
    vm.Initialize();
    vm.StartEvaluation = function () {
        vm.evaluation.analyzationState = 'creatingSystem';
        services.CreateSystem({ SystemId: vm.evaluation.systemId }, function createSuccess() {
            vm.evaluation.analyzationState = 'evaluating';
            services.EvaluateDataInMatlab({ SystemId: vm.evaluation.systemId }, function EvaluateSuccss(result) {
                vm.evaluation.analyzationState = 'done';
                vm.resultChart.chartdata.labels = [];
                vm.resultChart.chartdata.datasets[0].data = [];
                vm.resultChart.chartdata.datasets[1].data = [];
                var i = 0;
                for (key in result.data) {
                    i++;
                    vm.resultChart.chartdata.labels.push(i);
                    vm.resultChart.chartdata.datasets[0].data.push(result.data[key].TrueValue);
                    vm.resultChart.chartdata.datasets[1].data.push(result.data[key].Estimation);
                }
            });
        });
    }
});