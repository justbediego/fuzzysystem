﻿angular.module("wit2fess").controller('homeCtrl', function ($scope, services, $uibModal, $state) {
    $scope.loginUser;
    //check if logged in
    services.IsLoggedIn(function success(result) {
        if (result.data) {
            $state.go('user.systems');
        };
    });
    $scope.Login = function () {
        if ($scope.loginUser && $scope.loginUser.Username && $scope.loginUser.Password) {
            services.Login($scope.loginUser, function success(result) {
                $state.go('user.systems');
            });
        } else
            services.ShowError("Input Information", "Please enter all the required fields");
    };
});
