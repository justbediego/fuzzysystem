﻿angular.module("wit2fess").controller('registerCtrl', function ($scope, services, $uibModal, $state) {
    $scope.registerUser = {};
    //check if logged in
    services.IsLoggedIn(function success(result) {
        if (result.data) {
            $state.go('user.systems');
        };
    });
    $scope.CreateUser = function () {
        if ($scope.PasswordCopy !== $scope.Password)
        {
            services.ShowError("Input Error", "Passwords do not match");
            return;
        }
        if ($scope.registerUser &&
            $scope.registerUser.Username &&
            $scope.registerUser.Password &&
            $scope.registerUser.FirstName &&
            $scope.registerUser.LastName) {
            services.CreateUser($scope.registerUser, function success(result) {
                $state.go('user.systems');
            });
        } else
            services.ShowError("Input Error", "Please enter all the required fields");
    };
});
