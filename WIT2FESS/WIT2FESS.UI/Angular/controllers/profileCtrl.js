﻿angular.module("wit2fess").controller('profileCtrl', function ($rootScope, $scope, services, $location, $uibModal, $state) {
    var vm = $scope;
    services.IsLoggedIn(function success(result) {
        if (!result.data)
            $state.go("home");
        else {
            vm.menuTempleteUrl = '/Statics/User/menu.html';
            services.GetUserData(function success(result) {
                vm.fullName = result.data.FirstName + " " + result.data.LastName;
            });
        }
    });
    vm.Logout = function () {
        services.Logout(function success(result) {
            $state.go("home");
        });
    };
});