﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIT2FESS.UI.Models
{
    public class EditOrAddSystemDTO
    {
        public int? SystemId;
        public string SystemName;
        public int AndMethod;
        public int OrMethod;
        public int ImplicationMethod;
        public int AggregationMethod;
        public int DefuzzificationMethod;
        public int TypeReductionMethod;
    }
}