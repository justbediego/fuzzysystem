﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIT2FESS.UI.Models
{
    public class EditOrAddVariableDTO
    {
        public int? VariableId;
        public int SystemId;
        public string VariableName;
        //public int VariableIndex;
        public int VariableType;
        public double RangeMax;
        public double RangeMin;
    }
}