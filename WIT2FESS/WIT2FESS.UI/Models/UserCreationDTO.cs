﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIT2FESS.UI.Models
{
    public class UserCreationDTO
    {
        public string Username;
        public string Password;
        public string FirstName;
        public string LastName;
    }
}