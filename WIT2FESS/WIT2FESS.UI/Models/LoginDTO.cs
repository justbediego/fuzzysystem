﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIT2FESS.UI.Models
{
    public class LoginDTO 
    {
        public string Username;
        public string Password;
        public bool IsPersistent;
    }
}