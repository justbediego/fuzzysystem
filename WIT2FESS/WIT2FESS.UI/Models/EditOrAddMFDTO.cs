﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WIT2FESS.UI.Models
{
    public class EditOrAddMFDTO
    {
        public int? MFId;
        public int VariableId;
        public string MFName;
        public int MFType;
        public string Intervals;
        public double FOU;
    }
}