﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WIT2FESS.Core.DataAccess;
using WIT2FESS.Core.Model;
using System.Data.Entity;
using Newtonsoft.Json;
using WIT2FESS.Core.Model.DTO;
using System.IO;

namespace WIT2FESS.Core.Business
{
    public class UserBusiness
    {
        private WIT2FESSContext dbContext;
        private GuestBusiness guestBusiness;
        public UserBusiness(WIT2FESSContext dbContext)
        {
            this.dbContext = dbContext;
            guestBusiness = new GuestBusiness(dbContext);
        }
        public string GetEvaluationOutputFileName(string parentFolder, string systemName, bool extension = true)
        {
            return Path.Combine(parentFolder, systemName + "output" + (extension ? ".csv" : ""));
        }
        private string GetEvaluationFileName(string username, string parentFolder, bool extension = true)
        {
            return Path.Combine(parentFolder, "Evaluation_" + username + (extension ? ".csv" : ""));
        }
        private string GetSystemFileName(int userId, int systemId, string parentFolder, bool extension = true)
        {
            return Path.Combine(parentFolder, string.Format("{0}_{1}_system" + (extension ? ".csv" : ""), userId, systemId));
        }
        private string GetVariablesFileName(int userId, int systemId, string parentFolder, bool extension = true)
        {
            return Path.Combine(parentFolder, string.Format("{0}_{1}_variables" + (extension ? ".csv" : ""), userId, systemId));
        }
        private string GetRulesFileName(int userId, int systemId, string parentFolder, bool extension = true)
        {
            return Path.Combine(parentFolder, string.Format("{0}_{1}_rules" + (extension ? ".csv" : ""), userId, systemId));
        }

        public void EditOrAddSystem(string username, int? systemId, string systemName, FIS.AndMethods andMethod, FIS.OrMethods orMethod, FIS.ImplicationMethods implicationMethod, FIS.AggregationMethods aggregationMethod, FIS.DefuzzificationMethods defuzzificationMethod, FIS.TypeReductionMethods typeReductionMethod)
        {
            if (systemName.Length < 2 || systemName.Length > 100)
                throw new BusinessException("System name too long or too short");
            var user = guestBusiness.GetUser(username);
            FIS fis = null;
            if (systemId.HasValue)
            {
                fis = dbContext.FISs.SingleOrDefault(f => f.ID == systemId && f.User_ID == user.ID);
                if (fis == null)
                    throw new BusinessException("System not found");
            }
            else
            {
                fis = new FIS() { User_ID = user.ID };
                dbContext.FISs.Add(fis);
            }
            fis.Name = systemName;
            fis.AndMethod = andMethod;
            fis.AggregationMethod = aggregationMethod;
            fis.DefuzzificationMethod = defuzzificationMethod;
            fis.ImplicationMethod = implicationMethod;
            fis.OrMethod = orMethod;
            fis.TypeReductionMethod = typeReductionMethod;
            dbContext.SaveChanges();
        }
        public void RemoveSystem(string username, int systemId)
        {
            var user = guestBusiness.GetUser(username);
            FIS fis = dbContext.FISs.Include(f => f.Variables).Include(f => f.Rules).SingleOrDefault(f => f.User_ID == user.ID && f.ID == systemId);
            if (fis == null)
                throw new BusinessException("System not found");
            if (fis.Variables.Any())
                throw new BusinessException("Some Variables are dependant to this System");
            if (fis.Rules.Any())
                throw new BusinessException("Some Rules are dependant to this System");
            dbContext.FISs.Remove(fis);
            dbContext.SaveChanges();
        }
        public void EditOrAddVariable(string username, int? variableId, int systemId, string variableName, Variable.VariableTypes variableType, double rangeMax, double rangeMin)
        {
            if (variableName.Length < 2 || variableName.Length > 100)
                throw new BusinessException("Variable name too long or too short");
            if (rangeMin > rangeMax)
                throw new BusinessException("Range Min greater than Max");
            var user = guestBusiness.GetUser(username);
            FIS system = dbContext.FISs.Where(f => f.ID == systemId && f.User_ID == user.ID).Include(f => f.Variables).SingleOrDefault();
            if (system == null)
                throw new BusinessException("System not found");
            Variable variable = null;
            if (variableId.HasValue)
            {
                variable = dbContext.Variables.SingleOrDefault(v => v.ID == variableId && v.FIS_ID == system.ID);
                if (variable == null)
                    throw new BusinessException("Variable not found");
            }
            else
            {
                variable = new Variable() { VariableType = Variable.VariableTypes.Input, FIS_ID = system.ID };
                dbContext.Variables.Add(variable);
            }
            // previous value
            if (variable.VariableType != Variable.VariableTypes.Output && variableType == Variable.VariableTypes.Output)
            {
                if (system.Variables.Any(v => v.VariableType == Variable.VariableTypes.Output))
                    throw new BusinessException("System already has an output variable");
            }
            variable.Name = variableName;
            variable.RangeMax = rangeMax;
            variable.RangeMin = rangeMin;
            variable.VariableType = variableType;
            //variable.VarIndex = variableIndex;
            dbContext.SaveChanges();
        }
        public void RemoveVariable(string username, int variableId)
        {
            var user = guestBusiness.GetUser(username);
            Variable variable = dbContext.Variables.Include(v => v.MFList).SingleOrDefault(v => v.ID == variableId && v.FIS.User_ID == user.ID);
            if (variable == null)
                throw new BusinessException("Variable not found");
            if (variable.MFList.Any())
                throw new BusinessException("Some MFs are dependant to this Variable");
            dbContext.Variables.Remove(variable);
            dbContext.SaveChanges();
        }
        public void EditOrAddMF(string username, int? mfId, int variableId, string mfName, MF.MFTypes mfType, string intervals, double fou)
        {
            List<double> deserializedIntervals = null;
            try
            {
                deserializedIntervals = JsonConvert.DeserializeObject<List<double>>(intervals);
            }
            catch
            {
                throw new BusinessException("Invalid intervals values. Should be in shape of [a,b,..]");
            }
            if (mfType == MF.MFTypes.Triangle && deserializedIntervals.Count != 3)
                throw new BusinessException("There should be exactly 3 intervals");
            if (mfType == MF.MFTypes.Trapezoid && deserializedIntervals.Count != 4)
                throw new BusinessException("There should be exactly 4 intervals");
            if (fou < 0 || fou > 1)
                throw new BusinessException("FOU should be between 0 and 1");
            var user = guestBusiness.GetUser(username);
            Variable variable = dbContext.Variables.Where(v => v.ID == variableId && v.FIS.User_ID == user.ID).Include(v => v.MFList).SingleOrDefault();
            if (variable == null)
                throw new BusinessException("Variable not found");
            if (deserializedIntervals.Any(i => i < variable.RangeMin || i > variable.RangeMax))
                throw new BusinessException("Intervals should be in the range of their variable");
            if (!deserializedIntervals.SequenceEqual(deserializedIntervals.OrderBy(i => i)))
                throw new BusinessException("Intervals should be in increasing order");
            MF mf = null;
            if (mfId.HasValue)
            {
                mf = dbContext.MFs.SingleOrDefault(m => m.ID == mfId && m.Variable_ID == variable.ID);
                if (mf == null)
                    throw new BusinessException("Membership Function not found");
            }
            else
            {
                mf = new MF() { Variable_ID = variable.ID };
                dbContext.MFs.Add(mf);
            }
            mf.Intervals = intervals;
            // id is incremental, not needed
            //mf.MFIndex = ???
            mf.MFType = mfType;
            mf.FOU = fou;
            mf.Name = mfName;
            dbContext.SaveChanges();
        }
        public void RemoveMF(string username, int mfId)
        {
            var user = guestBusiness.GetUser(username);
            MF mf = dbContext.MFs.Where(m => m.ID == mfId && m.Variable.FIS.User_ID == user.ID).Include(m => m.RuleMFs).SingleOrDefault();
            if (mf == null)
                throw new BusinessException("MF not found");
            if (mf.RuleMFs.Any())
                throw new BusinessException("Some rules are dependant to this MF");
            dbContext.MFs.Remove(mf);
            dbContext.SaveChanges();
        }
        public int AddARule(string username, int systemId)
        {
            var user = guestBusiness.GetUser(username);
            FIS system = dbContext.FISs.Where(f => f.ID == systemId && f.User_ID == user.ID).Include(f => f.Variables.Select(v => v.MFList)).SingleOrDefault();
            if (system == null)
                throw new BusinessException("System not found");
            if (system.Variables.Count(v => v.VariableType == Variable.VariableTypes.Output) != 1)
                throw new BusinessException("System does not have 1 output variable");
            if (!system.Variables.Any(v => v.VariableType == Variable.VariableTypes.Input))
                throw new BusinessException("System does not have any input variable");
            if (!system.Variables.All(v => v.MFList.Count > 1))
                throw new BusinessException("Not all variables have at least 2 membership functions");
            Rule rule = new Rule()
            {
                FIS_ID = system.ID,
            };
            dbContext.Rules.Add(rule);
            dbContext.SaveChanges();
            return rule.ID;
        }
        public void RemoveRule(string username, int ruleId)
        {
            var user = guestBusiness.GetUser(username);
            Rule rule = dbContext.Rules.Where(r => r.ID == ruleId && r.FIS.User_ID == user.ID).SingleOrDefault();
            if (rule == null)
                throw new BusinessException("Rule not found");
            dbContext.Rules.Remove(rule);
            dbContext.SaveChanges();
        }
        public void AddMFToRule(string username, int ruleId, int mfId)
        {
            var user = guestBusiness.GetUser(username);
            Rule rule = dbContext.Rules.Where(r => r.ID == ruleId && r.FIS.User_ID == user.ID).Include(r => r.RuleMFs.Select(rmf => rmf.MF.Variable)).Include(r => r.FIS).SingleOrDefault();
            if (rule == null)
                throw new BusinessException("Rule not found");
            MF mf = dbContext.MFs.Where(m => m.ID == mfId && m.Variable.FIS_ID == rule.FIS_ID).Include(m => m.Variable).SingleOrDefault();
            if (mf == null)
                throw new BusinessException("Membership Function not found");
            if (rule.RuleMFs.Any(rmf => rmf.MF.Variable_ID == mf.Variable_ID))
                throw new BusinessException("There is already a MF for this variable in this Rule");
            rule.RuleMFs.Add(new RuleMF()
            {
                MF_ID = mf.ID,
            });
            dbContext.SaveChanges();
        }
        public void RemoveMFFromRule(string username, int ruleId, int mfId)
        {
            var user = guestBusiness.GetUser(username);
            var ruleMF = dbContext.RuleMFs.Where(rmf => rmf.MF_ID == mfId && rmf.Rule_ID == ruleId && rmf.Rule.FIS.User_ID == user.ID).SingleOrDefault();
            if (ruleMF == null)
                throw new BusinessException("Releation was not found");
            dbContext.RuleMFs.Remove(ruleMF);
            dbContext.SaveChanges();
        }
        public void CreateSystemInMatlab(string username, string matFolder, int systemId)
        {
            var user = guestBusiness.GetUser(username);
            FIS fis = dbContext.FISs
                .Include(f => f.Variables.Select(v => v.MFList))
                .Include(f => f.Rules.Select(r => r.RuleMFs.Select(rmf => rmf.MF.Variable)))
                .SingleOrDefault(f => f.User_ID == user.ID && f.ID == systemId);
            if (fis == null)
                throw new BusinessException("System not found");
            if (!fis.Rules.Any())
                throw new BusinessException("System does not have any rules");
            if (!fis.Rules.All(r => r.RuleMFs.Any(rmf => rmf.MF.Variable.VariableType == Variable.VariableTypes.Output)))
                throw new BusinessException("Not all rules have an output");
            if (!fis.Rules.All(r => r.RuleMFs.Any(rmf => rmf.MF.Variable.VariableType == Variable.VariableTypes.Input)))
                throw new BusinessException("Not all rules have an input");
            //write system
            if (File.Exists(GetSystemFileName(user.ID, fis.ID, matFolder)))
                File.Delete(GetSystemFileName(user.ID, fis.ID, matFolder));
            string fileData = "fisName,fisType,andMethod,orMethod,impMethod,aggMethod,defuzzMethod,Trmethod\n" + fis.Name + ",mamdani," + fis.Matlab_AndMethod + "," + fis.Matlab_OrMethod + "," + fis.Matlab_ImplicationMethod + "," + fis.Matlab_AggregationMethod + "," + fis.Matlab_DefuzzificationMethod + "," + fis.Matlab_TypeReductionMethod;
            File.WriteAllText(GetSystemFileName(user.ID, fis.ID, matFolder), fileData);
            //write variables
            var variablesOrdered = fis.Variables.OrderBy(v => v.VariableType).ThenBy(v => v.ID).ToList();
            if (File.Exists(GetVariablesFileName(user.ID, fis.ID, matFolder)))
                File.Delete(GetVariablesFileName(user.ID, fis.ID, matFolder));
            fileData = "varName,varType,varIndex,varBound,Mfname,Mftype,intervals,FOU\n";
            for (int i = 0; i < variablesOrdered.Count; i++)
            {
                var variable = variablesOrdered[i];
                foreach (var mf in variable.MFList.OrderBy(m => m.ID))
                    fileData += variable.Name + "," + variable.Matlab_VariableType + "," + (variable.VariableType == Variable.VariableTypes.Input ? (i + 1) : 1) + ",\"[" + variable.RangeMin + "," + variable.RangeMax + "]\"," + mf.Name + "," + mf.Matlab_MFType + ",\"" + mf.Intervals + "\"," + mf.FOU + "\n";
            }
            File.WriteAllText(GetVariablesFileName(user.ID, fis.ID, matFolder), fileData);
            //write rules
            if (File.Exists(GetRulesFileName(user.ID, fis.ID, matFolder)))
                File.Delete(GetRulesFileName(user.ID, fis.ID, matFolder));
            fileData = "";
            foreach (var variable in variablesOrdered)
                fileData += "\"" + variable.Matlab_VariableType + ": " + variable.Name + "\",";
            fileData = fileData.Substring(0, fileData.Length - 1) + "\n";
            foreach (var rule in fis.Rules)
            {
                foreach (var variable in variablesOrdered)
                {
                    int? firstMatch = rule.RuleMFs.Where(rmf => rmf.MF.Variable_ID == variable.ID).Select(rmf => (int?)rmf.MF_ID).FirstOrDefault();
                    int? matchIndex = firstMatch == null ? null : (int?)variable.MFList.OrderBy(mf => mf.ID).Select(mf => mf.ID).ToList().IndexOf(firstMatch.Value);
                    fileData += (matchIndex.HasValue ? (matchIndex + 1).ToString() : "0") + ",";
                }
                fileData = fileData.Substring(0, fileData.Length - 1) + "\n";
            }
            File.WriteAllText(GetRulesFileName(user.ID, fis.ID, matFolder), fileData);
            //Call matlba
            matlabfuncs.Class1 fuzzyClass = new matlabfuncs.Class1();
            fuzzyClass.createFIS2(0,
                matFolder,
                GetSystemFileName(user.ID, fis.ID, matFolder, false),
                GetVariablesFileName(user.ID, fis.ID, matFolder, false),
                GetRulesFileName(user.ID, fis.ID, matFolder, false));
        }
        public List<EvaluationResultDTO> EvaluateDataInMatlab(string username, string matFolder, int systemId)
        {
            var user = guestBusiness.GetUser(username);
            FIS fis = dbContext.FISs
                .Include(f => f.Variables.Select(v => v.MFList))
                .Include(f => f.Rules.Select(r => r.RuleMFs.Select(rmf => rmf.MF.Variable)))
                .SingleOrDefault(f => f.User_ID == user.ID && f.ID == systemId);
            if (fis == null)
                throw new BusinessException("System not found");
            if (!fis.Rules.Any())
                throw new BusinessException("System does not have any rules");
            if (!fis.Rules.All(r => r.RuleMFs.Any(rmf => rmf.MF.Variable.VariableType == Variable.VariableTypes.Output)))
                throw new BusinessException("Not all rules have an output");
            if (!fis.Rules.All(r => r.RuleMFs.Any(rmf => rmf.MF.Variable.VariableType == Variable.VariableTypes.Input)))
                throw new BusinessException("Not all rules have an input");
            matlabfuncs.Class1 fuzzyClass = new matlabfuncs.Class1();
            fuzzyClass.evaluation2(0, matFolder, fis.Name, GetEvaluationFileName(username, matFolder, false));
            if (!File.Exists(GetEvaluationOutputFileName(matFolder, fis.Name)))
                throw new BusinessException("No output was generated");
            return Csv.CsvReader.ReadFromText(File.ReadAllText(GetEvaluationOutputFileName(matFolder, fis.Name))).Select(row => new EvaluationResultDTO()
            {
                TrueValue=double.Parse(row.Values[row.Values.Length-2]),
                Estimation=double.Parse(row.Values[row.Values.Length-1])
            }).ToList();
        }
        ///Get
        ///
        public List<SystemDTO> GetAllSystems(string username)
        {
            var user = guestBusiness.GetUser(username);
            return dbContext.FISs.Where(s => s.User_ID == user.ID).Select(s => new SystemDTO()
            {
                AggregationMethod = ((int)s.AggregationMethod).ToString(),
                AndMethod = ((int)s.AndMethod).ToString(),
                DefuzzificationMethod = ((int)s.DefuzzificationMethod).ToString(),
                ImplicationMethod = ((int)s.ImplicationMethod).ToString(),
                OrMethod = ((int)s.OrMethod).ToString(),
                SystemId = s.ID,
                SystemName = s.Name,
                TypeReductionMethod = ((int)s.TypeReductionMethod).ToString()
            }).ToList();
        }
        public SystemDTO GetSystemByID(string username, int systemId)
        {
            var user = guestBusiness.GetUser(username);
            return dbContext.FISs.Where(s => s.User_ID == user.ID && s.ID == systemId).Select(s => new SystemDTO()
            {
                AggregationMethod = ((int)s.AggregationMethod).ToString(),
                AndMethod = ((int)s.AndMethod).ToString(),
                DefuzzificationMethod = ((int)s.DefuzzificationMethod).ToString(),
                ImplicationMethod = ((int)s.ImplicationMethod).ToString(),
                OrMethod = ((int)s.OrMethod).ToString(),
                SystemId = s.ID,
                SystemName = s.Name,
                TypeReductionMethod = ((int)s.TypeReductionMethod).ToString()
            }).FirstOrDefault();
        }
        public List<VariableDTO> GetAllVariables(string username, int systemId)
        {
            var user = guestBusiness.GetUser(username);
            return dbContext.Variables.Where(v => v.FIS.User_ID == user.ID && v.FIS_ID == systemId).OrderBy(v => v.VariableType).ThenBy(v => v.ID).Select(v => new VariableDTO()
            {
                VariableName = v.Name,
                RangeMax = v.RangeMax,
                RangeMin = v.RangeMin,
                VariableID = v.ID,
                VariableType = ((int)v.VariableType).ToString(),
                //VariableIndex = v.VarIndex,
                MFList = v.MFList.Select(mf => new MFDTO()
                {
                    MFName = mf.Name,
                    FOU = mf.FOU,
                    Intervals = mf.Intervals,
                    MFId = mf.ID,
                    MFIndex = mf.MFIndex,
                    MFType = ((int)mf.MFType).ToString()
                }).ToList()
            }).ToList();
        }
        public VariableDTO GetVariableByID(string username, int variableId)
        {
            var user = guestBusiness.GetUser(username);
            return dbContext.Variables.Where(v => v.FIS.User_ID == user.ID && v.ID == variableId).Select(v => new VariableDTO()
            {
                VariableName = v.Name,
                RangeMax = v.RangeMax,
                RangeMin = v.RangeMin,
                VariableID = v.ID,
                VariableType = ((int)v.VariableType).ToString(),
                //VariableIndex = v.VarIndex,
                MFList = v.MFList.Select(mf => new MFDTO()
                {
                    MFName = mf.Name,
                    FOU = mf.FOU,
                    Intervals = mf.Intervals,
                    MFId = mf.ID,
                    MFIndex = mf.MFIndex,
                    MFType = ((int)mf.MFType).ToString()
                }).ToList()
            }).FirstOrDefault();
        }
        public MFDTO GetMFByID(string username, int mfId)
        {
            var user = guestBusiness.GetUser(username);
            return dbContext.MFs.Where(mf => mf.Variable.FIS.User_ID == user.ID && mf.ID == mfId).Select(mf => new MFDTO()
            {
                FOU = mf.FOU,
                Intervals = mf.Intervals,
                MFId = mf.ID,
                MFIndex = mf.MFIndex,
                MFType = ((int)mf.MFType).ToString(),
                MFName = mf.Name,
            }).FirstOrDefault();
        }
        public List<RuleDTO> GetAllRules(string username, int systemId)
        {
            var user = guestBusiness.GetUser(username);
            return dbContext.Rules.Where(r => r.FIS.User_ID == user.ID && r.FIS_ID == systemId).Select(r => new RuleDTO()
            {
                RuleID = r.ID,
                RuleMFs = r.RuleMFs.Select(rmf => new RuleMFDTO()
                {
                    MFId = rmf.MF_ID,
                    MFName = rmf.MF.Name,
                    VariableID = rmf.MF.Variable.ID,
                    VariableName = rmf.MF.Variable.Name,
                    VariableType = ((int)rmf.MF.Variable.VariableType).ToString(),
                }).ToList(),
            }).ToList();
        }
    }
}
