﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WIT2FESS.Core.DataAccess;
using WIT2FESS.Core.Model;

namespace WIT2FESS.Core.Business
{
    public class GuestBusiness
    {
        private WIT2FESSContext dbContext;
        public GuestBusiness(WIT2FESSContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public void CreateUser(string username, string password, string firstName, string lastName)
        {
            if (username.Length < 6)
                throw new BusinessException("Username length less than 6");
            if (username.Length > 100)
                throw new BusinessException("Username length less more than 100");
            if (password.Length < 6)
                throw new BusinessException("Password length less than 6");
            if (password.Length > 100)
                throw new BusinessException("Password length less more than 100");
            if (dbContext.Users.Any(u => u.Username == username.ToLower()))
                throw new BusinessException("Username already taken");
            dbContext.Users.Add(new User()
            {
                Username = username.ToLower(),
                Password = password,
                FirstName = firstName,
                LastName = lastName,
            });
            dbContext.SaveChanges();
        }
        public User GetUser(string username)
        {
            User user = dbContext.Users.SingleOrDefault(u => u.Username == username.ToLower());
            if (user == null)
                throw new BusinessException("User not found");
            return user;
        }
        public User GetUser(string username, string password)
        {
            User user = dbContext.Users.SingleOrDefault(u => u.Username == username.ToLower() && u.Password == password);
            if (user == null)
                throw new BusinessException("Username or Password wrong!");
            return user;
        }
    }
}
