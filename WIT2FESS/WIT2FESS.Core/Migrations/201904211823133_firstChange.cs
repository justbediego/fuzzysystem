namespace WIT2FESS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstChange : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Username, unique: true);
            
            CreateTable(
                "dbo.FIS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        User_ID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        AndMethod = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_ID, cascadeDelete: true)
                .Index(t => t.User_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FIS", "User_ID", "dbo.Users");
            DropIndex("dbo.FIS", new[] { "User_ID" });
            DropIndex("dbo.Users", new[] { "Username" });
            DropTable("dbo.FIS");
            DropTable("dbo.Users");
        }
    }
}
