namespace WIT2FESS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newChanges3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Variables", "VarIndex", c => c.Int(nullable: false));
            AddColumn("dbo.MFs", "MFIndex", c => c.Int(nullable: false));
            DropColumn("dbo.Variables", "Index");
            DropColumn("dbo.MFs", "Index");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MFs", "Index", c => c.Int(nullable: false));
            AddColumn("dbo.Variables", "Index", c => c.Int(nullable: false));
            DropColumn("dbo.MFs", "MFIndex");
            DropColumn("dbo.Variables", "VarIndex");
        }
    }
}
