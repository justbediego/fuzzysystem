namespace WIT2FESS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newChanges2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "FirstName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.Users", "LastName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.Variables", "Index", c => c.Int(nullable: false));
            AddColumn("dbo.MFs", "Index", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MFs", "Index");
            DropColumn("dbo.Variables", "Index");
            DropColumn("dbo.Users", "LastName");
            DropColumn("dbo.Users", "FirstName");
        }
    }
}
