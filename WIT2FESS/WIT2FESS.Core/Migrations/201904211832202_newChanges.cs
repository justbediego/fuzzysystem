namespace WIT2FESS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FIS", "OrMethod", c => c.Int(nullable: false));
            AddColumn("dbo.FIS", "ImplicationMethod", c => c.Int(nullable: false));
            AddColumn("dbo.FIS", "AggregationMethod", c => c.Int(nullable: false));
            AddColumn("dbo.FIS", "DefuzzificationMethod", c => c.Int(nullable: false));
            AddColumn("dbo.FIS", "TypeReductionMethod", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FIS", "TypeReductionMethod");
            DropColumn("dbo.FIS", "DefuzzificationMethod");
            DropColumn("dbo.FIS", "AggregationMethod");
            DropColumn("dbo.FIS", "ImplicationMethod");
            DropColumn("dbo.FIS", "OrMethod");
        }
    }
}
