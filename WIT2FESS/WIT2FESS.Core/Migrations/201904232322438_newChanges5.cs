namespace WIT2FESS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newChanges5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MFs", "FOU", c => c.Double(nullable: false));
            DropColumn("dbo.MFs", "FOUs");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MFs", "FOUs", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.MFs", "FOU");
        }
    }
}
