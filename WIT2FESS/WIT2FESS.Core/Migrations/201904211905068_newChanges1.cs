namespace WIT2FESS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newChanges1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Variables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FIS_ID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        VariableType = c.Int(nullable: false),
                        RangeMin = c.Double(nullable: false),
                        RangeMax = c.Double(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FIS", t => t.FIS_ID, cascadeDelete: true)
                .Index(t => t.FIS_ID);
            
            CreateTable(
                "dbo.MFs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Variable_ID = c.Int(nullable: false),
                        MFType = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Intervals = c.String(nullable: false, maxLength: 100),
                        FOUs = c.String(nullable: false, maxLength: 100),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Variables", t => t.Variable_ID, cascadeDelete: true)
                .Index(t => t.Variable_ID);
            
            CreateTable(
                "dbo.RuleMFs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Rule_ID = c.Int(nullable: false),
                        MF_ID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MFs", t => t.MF_ID, cascadeDelete: true)
                .ForeignKey("dbo.Rules", t => t.Rule_ID, cascadeDelete: true)
                .Index(t => t.Rule_ID)
                .Index(t => t.MF_ID);
            
            CreateTable(
                "dbo.Rules",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        User_ID = c.Int(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.User_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MFs", "Variable_ID", "dbo.Variables");
            DropForeignKey("dbo.Rules", "User_ID", "dbo.Users");
            DropForeignKey("dbo.RuleMFs", "Rule_ID", "dbo.Rules");
            DropForeignKey("dbo.RuleMFs", "MF_ID", "dbo.MFs");
            DropForeignKey("dbo.Variables", "FIS_ID", "dbo.FIS");
            DropIndex("dbo.Rules", new[] { "User_ID" });
            DropIndex("dbo.RuleMFs", new[] { "MF_ID" });
            DropIndex("dbo.RuleMFs", new[] { "Rule_ID" });
            DropIndex("dbo.MFs", new[] { "Variable_ID" });
            DropIndex("dbo.Variables", new[] { "FIS_ID" });
            DropTable("dbo.Rules");
            DropTable("dbo.RuleMFs");
            DropTable("dbo.MFs");
            DropTable("dbo.Variables");
        }
    }
}
