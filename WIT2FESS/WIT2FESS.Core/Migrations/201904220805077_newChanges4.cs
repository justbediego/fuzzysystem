namespace WIT2FESS.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newChanges4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rules", "User_ID", "dbo.Users");
            DropIndex("dbo.Rules", new[] { "User_ID" });
            AddColumn("dbo.Rules", "FIS_ID", c => c.Int());
            AlterColumn("dbo.MFs", "MFIndex", c => c.Int());
            CreateIndex("dbo.Rules", "FIS_ID");
            AddForeignKey("dbo.Rules", "FIS_ID", "dbo.FIS", "ID");
            DropColumn("dbo.Rules", "User_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rules", "User_ID", c => c.Int());
            DropForeignKey("dbo.Rules", "FIS_ID", "dbo.FIS");
            DropIndex("dbo.Rules", new[] { "FIS_ID" });
            AlterColumn("dbo.MFs", "MFIndex", c => c.Int(nullable: false));
            DropColumn("dbo.Rules", "FIS_ID");
            CreateIndex("dbo.Rules", "User_ID");
            AddForeignKey("dbo.Rules", "User_ID", "dbo.Users", "ID");
        }
    }
}
