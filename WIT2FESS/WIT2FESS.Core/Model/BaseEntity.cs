﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model
{
    public abstract class BaseEntity
    {
        [Key]
        public int ID { get; set; }
        public DateTime DateCreated { get; set; }
        public BaseEntity()
        {
            DateCreated = DateTime.Now;
        }
    }
}
