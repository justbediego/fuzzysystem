﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model
{
    public class Rule:BaseEntity
    {
        //this is supposed to be required but circular_cascade doesn't allow it
        public int? FIS_ID { get; set; }
        [ForeignKey("FIS_ID")]
        public FIS FIS{ get; set; }
        public List<RuleMF> RuleMFs { get; set; }
        public Rule()
        {
            RuleMFs = new List<RuleMF>();
        }
    }
}
