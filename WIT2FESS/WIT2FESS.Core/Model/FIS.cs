﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model
{
    public class FIS : BaseEntity
    {
        public enum AndMethods
        {
            Minimum = 0,
            Product = 1,
        }
        public enum OrMethods
        {
            Maximum = 0,
            ProbabilisticOR = 1,
        }
        public enum ImplicationMethods
        {
            Minimum = 0,
            Product = 1,
        }
        public enum AggregationMethods
        {
            Maximum = 0,
            ProbabilisticOR = 1,
            Sum = 2,
        }
        public enum DefuzzificationMethods
        {
            CentroidAverage = 0,
            Bisector = 1,
        }
        public enum TypeReductionMethods
        {
            Karnik_Mendel = 0,
            Wu_Tan = 1,
            Greenfield_Chiclana_Coupland_John = 2,
        }
        public int User_ID { get; set; }
        [ForeignKey("User_ID")]
        public User User { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Name { get; set; }
        public AndMethods AndMethod { get; set; }
        public OrMethods OrMethod { get; set; }
        public ImplicationMethods ImplicationMethod { get; set; }
        public AggregationMethods AggregationMethod { get; set; }
        public DefuzzificationMethods DefuzzificationMethod { get; set; }
        public TypeReductionMethods TypeReductionMethod { get; set; }
        public List<Variable> Variables { get; set; }
        public List<Rule> Rules { get; set; }
        [NotMapped]
        public string Matlab_AndMethod { get { return AndMethod == AndMethods.Minimum ? "min" : "prod"; } }
        [NotMapped]
        public string Matlab_OrMethod { get { return OrMethod == OrMethods.Maximum ? "max" : "probor"; } }
        [NotMapped]
        public string Matlab_ImplicationMethod { get { return ImplicationMethod == ImplicationMethods.Minimum ? "min" : "probor"; } }
        [NotMapped]
        public string Matlab_DefuzzificationMethod { get { return DefuzzificationMethod == DefuzzificationMethods.CentroidAverage ? "centroid" : "bisector"; } }
        [NotMapped]
        public string Matlab_TypeReductionMethod
        {
            get
            {
                switch (TypeReductionMethod)
                {
                    case TypeReductionMethods.Karnik_Mendel:
                        return "KM";
                    case TypeReductionMethods.Wu_Tan:
                        return "WT";
                    default:
                        return "sum";
                }
            }
        }
        [NotMapped]
        public string Matlab_AggregationMethod
        {
            get
            {
                switch (AggregationMethod)
                {
                    case AggregationMethods.Maximum:
                        return "max";
                    case AggregationMethods.ProbabilisticOR:
                        return "probor";
                    default:
                        return "GCCJ";
                }
            }
        }

        public FIS()
        {
            Variables = new List<Variable>();
            Rules = new List<Rule>();
        }
    }
}
