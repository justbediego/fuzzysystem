﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model
{
    public class User : BaseEntity
    {
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string LastName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [StringLength(100, MinimumLength = 6)]
        public string Username { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Password { get; set; }
        public List<FIS> FISList { get; set; }        
        public User()
        {
            FISList = new List<FIS>();
        }
    }
}
