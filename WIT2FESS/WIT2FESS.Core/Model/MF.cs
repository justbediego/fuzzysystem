﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model
{
    public class MF : BaseEntity
    {
        public int Variable_ID { get; set; }
        [ForeignKey("Variable_ID")]
        public Variable Variable { get; set; }
        public enum MFTypes
        { 
            Triangle = 0,
            Trapezoid = 1
        }
        public MFTypes MFType { get; set; }
        [NotMapped]
        public string Matlab_MFType { get{ return MFType == MFTypes.Triangle ? "trimf" : "trapmf"; } }
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Name { get; set; }
        public int? MFIndex { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 1)]
        //must be in shape 1,2,3,4,..
        public string Intervals { get; set; }
        public double FOU { get; set; }
        public List<RuleMF> RuleMFs { get; set; }
        public MF()
        {
            RuleMFs = new List<RuleMF>();
        }
    }
}
