﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model.DTO
{
    public class RuleMFDTO
    {
        public int MFId;
        public string MFName;
        public int VariableID;
        public string VariableName;
        public string VariableType;
    }
}
