﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model.DTO
{
    public class SystemDTO
    {
        public int SystemId;
        public string SystemName;
        //because of angular
        public string AndMethod;
        public string OrMethod;
        public string ImplicationMethod;
        public string AggregationMethod;
        public string DefuzzificationMethod;
        public string TypeReductionMethod;
    }
}
