﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model.DTO
{
    public class EvaluationResultDTO
    {
        public double TrueValue;
        public double Estimation;
    }
}
