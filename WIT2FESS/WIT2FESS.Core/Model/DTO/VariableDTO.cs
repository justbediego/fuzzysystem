﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model.DTO
{
    public class VariableDTO
    {
        public int VariableID;
        public string VariableName;
        //public int VariableIndex;
        //because of angular
        public string VariableType;
        public double RangeMin;
        public double RangeMax;
        public List<MFDTO> MFList;
        public VariableDTO()
        {
            MFList = new List<MFDTO>();
        }
    }
}
