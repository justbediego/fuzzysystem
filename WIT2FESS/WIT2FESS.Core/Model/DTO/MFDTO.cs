﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model.DTO
{
    public class MFDTO
    {
        public int MFId;
        //because of angular
        public string MFType;
        public string MFName;
        public int? MFIndex;
        public string Intervals;
        public double FOU;
    }
}
