﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model.DTO
{
    public class RuleDTO
    {
        public int RuleID;
        public List<RuleMFDTO> RuleMFs;
        public RuleDTO()
        {
            RuleMFs = new List<RuleMFDTO>();
        }
    }
}
