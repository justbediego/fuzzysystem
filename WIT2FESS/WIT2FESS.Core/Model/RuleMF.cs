﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model
{
    public class RuleMF : BaseEntity
    {
        public int Rule_ID { get; set; }
        [ForeignKey("Rule_ID ")]
        public Rule Rule { get; set; }
        public int MF_ID { get; set; }
        [ForeignKey("MF_ID")]
        public MF MF { get; set; }
    }
}
