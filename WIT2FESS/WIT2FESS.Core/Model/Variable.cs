﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIT2FESS.Core.Model
{
    public class Variable : BaseEntity
    {
        public int FIS_ID { get; set; }
        [ForeignKey("FIS_ID")]
        public FIS FIS { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Name { get; set; }
        public int VarIndex { get; set; }
        public enum VariableTypes
        {
            Input = 0,
            Output = 1,
        }
        [NotMapped]
        public string Matlab_VariableType { get { return VariableType == VariableTypes.Input ? "input" : "output"; } }
        public VariableTypes VariableType { get; set; }
        [Required]
        public double RangeMin { get; set; }
        [Required]
        public double RangeMax { get; set; }
        public List<MF> MFList { get; set; }
        public Variable()
        {
            MFList = new List<MF>();
        }
    }
}
