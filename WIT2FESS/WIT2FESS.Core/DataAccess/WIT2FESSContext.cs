﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WIT2FESS.Core.Model;

namespace WIT2FESS.Core.DataAccess
{
    public class WIT2FESSContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<FIS> FISs { get; set; }
        public DbSet<MF> MFs { get; set; }
        public DbSet<Rule> Rules { get; set; }
        public DbSet<Variable> Variables { get; set; }
        public DbSet<RuleMF> RuleMFs { get; set; }
    }
}
